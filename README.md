# Python for Everyone: Andrew's Repository

This repository holds codes used in Python for everyone taught at Carlow University in Fall 2023

## Project 1: Road Trip Computer

This project demonstrates variable names and user input

## Project 2: Might we be friends?

Exploration of the power of nested conditional logic.

## Project 3: Looping: While looping and conditional logic

Learning how to use loops properly
