# Andrew Hunter, 9/27/23, make a program to request a user to input a number and then have it be randomly guessed

import random as rand
import time

# ask for speed, name, and wiggle room
print("How fast do you want your race car go? (in mph)")
guess_goal = int(input())
print("What do you want to name the car?")
car_name = input()
print("How close does the random car generator have to be in order to be good enough?")
wiggle_room = int(input())
print("Initiate the car making process!")
print("-------------------")
# make values so I can count how many guesses and the random guess so the while statement can run
inner_counter = 0
random_guess = 0
# repeat until the random guess is within the wiggle room of desired speed
while random_guess < guess_goal-wiggle_room:
    random_guess = rand.randrange(guess_goal)
    inner_counter = inner_counter + 1
    print(car_name, " Mk.", inner_counter, ": ", random_guess)
    time.sleep(.50)
# spit results back out
print("-------------------")
print("The car making has finished, check out the statistics of the brand new ", car_name, " Mk.", inner_counter, "!")
print("")
difference = guess_goal-random_guess
print("The ", car_name, " Mk.", inner_counter, " goes ", random_guess, "mph, which is ", difference, "mph away from your original goal, ", guess_goal, "mph")



